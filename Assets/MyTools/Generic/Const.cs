﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Const
{
    #region CONFIGS
    public static string CONFIG_AD = "config_ads";
    public static float moveSpeed = 5;
    public static bool useBoom = false;
    public static bool autoExplode = false;
    public static int baseNumberBoom = 6;
    #endregion

    #region Keys
    public const string SCENE_HOME = "Home";
    public const string SCENE_GAME = "Game";
    #endregion
}
