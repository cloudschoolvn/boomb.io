﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalSetting : SingletonMonoBehaviour<GlobalSetting>
{
    #region Inspector Variables
    #endregion

    #region Member Variables
    private float _fps,_deltaTime =0;
    #endregion

    #region Unity Methods
    private void Awake()
    {
        instance = this;
        Application.targetFrameRate = 60;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        DontDestroyOnLoad(this);
    }
    private void Start()
    {
    }

    private void Update()
    {
        _deltaTime += (Time.deltaTime - _deltaTime) * 0.1f;
        _fps = 1.0f / _deltaTime;
    }
    #endregion

    #region Public Methods
    
    #endregion

    #region Private Methods
    #endregion
}

