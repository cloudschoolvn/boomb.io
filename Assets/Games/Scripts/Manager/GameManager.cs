﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : SingletonMonoBehaviour<GameManager>
{
    #region Inspector Variables
    public Transform mCam;
    public Player mPlayer;
    public Player[] bots;
    #endregion

    #region Member Variables
    #endregion

    #region Unity Methods
    private void Awake()
    {
        instance = this;       
    }
    private void Start()
    {
        GameEventManager.Instance.EvtPlayerDie += HandlePlayerDie;
        mPlayer.Init("Player", MapManager.Instance.RandomPosition());
        foreach (Player pl in bots)
        {
            pl.Init("Bot", MapManager.Instance.RandomPosition());
        }
        ReadyStartGame();
    }

    private void OnDestroy()
    {
        GameEventManager.Instance.EvtPlayerDie -= HandlePlayerDie;
    }
    #endregion

    #region Public Methods
    public void OnGameStart()
    {
        mPlayer.Active();
        foreach(Player pl in bots)
        {
            pl.Active();
        }
    }

    public void RevivePlayer()
    {
        mPlayer.Init("Player",MapManager.Instance.RandomPosition());        
        mPlayer.Active();
    }

    public void SetCameraFollowPlayer(bool isFollow)
    {
        if (isFollow)
        {
            mCam.parent = mPlayer.mTrans;
            mCam.localPosition = new Vector3(0, 11, -8.5f);
            mCam.localEulerAngles = new Vector3(55, 0, 0);
        }
        else
        {
            mCam.parent = null;
        }

    }
    #endregion

    #region Private Methods
    private void ReadyStartGame()
    {
        GameUIManager.Instance.PrepareStart();
    }

    private void HandlePlayerDie(object pl)
    {
        Player player = (Player)pl;
        if(!player.isBot)
        {
            RevivePopup.Instance.Show();
        }
        else
        {            
            player.Init("Bot", MapManager.Instance.RandomPosition());
            player.Active();
        }
        
    }

    
    #endregion
}
