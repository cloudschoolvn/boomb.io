﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUIManager : SingletonMonoBehaviour<GameUIManager>
{
    #region Inspector Variables
    public Text tvCenter;
    public VariableJoystick joystick;
    #endregion

    #region Member Variables
    #endregion

    #region Unity Methods
    private void Awake()
    {
        instance = this;
    }
    #endregion

    #region Public Methods
    public void SpeedUp()
    {
        Const.moveSpeed = Mathf.Clamp(Const.moveSpeed + 0.5f, 1, 20);
    }
    public void SpeedDown()
    {
        Const.moveSpeed = Mathf.Clamp(Const.moveSpeed - 0.5f, 1, 20);
    }
    public void UseBoomb()
    {
        Const.useBoom = !Const.useBoom;
    }

    public void InscreaseBoomb()
    {
        GameManager.Instance.mPlayer.PowUp();
    }


    public void PrepareStart()
    {
        joystick.gameObject.SetActive(false);
        tvCenter.enabled = true;
        StartCoroutine(CountDownTask(3, tvCenter, () =>
        {
            joystick.gameObject.SetActive(true);
            tvCenter.enabled = false;
            GameManager.Instance.OnGameStart();
        }));
    }
    #endregion

    #region Private Methods
    private IEnumerator CountDownTask(int time, Text tv, Action callback)
    {
        while (time > 0)
        {
            tv.text = time.ToString();
            yield return new WaitForSeconds(1);
            time -= 1;
        }
        callback?.Invoke();
    }
    #endregion   
}
