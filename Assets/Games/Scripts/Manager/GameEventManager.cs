﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEventManager : SingletonMonoBehaviour<GameEventManager>
{
    #region Events
    public ActionEvent.OneParam EvtPlayerDie;
    #endregion

    #region Methods
    public void OnPlayerDie(Player pl)
    {
        EvtPlayerDie?.Invoke(pl);
    }
    #endregion


    #region Unity Methods
    private void Awake()
    {
        instance = this;
    }
    private void OnDestroy()
    {
        EvtPlayerDie = null;
    }
    #endregion
}
