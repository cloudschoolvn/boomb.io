﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BotController : MonoBehaviour
{
    #region Inspector Variables
    public Transform player;
    public ColliderEvent centerEye, leftEye, rightEye, waterFront, waterLeft, waterRight;
    #endregion

    #region Member Variables
    private bool isActive;
    private int _level;   
    private Dictionary<int, Tile> groundInRange;
    private bool waterInFront = false, waterInLeft = false, waterInRight = false;
    private bool canMoveFront = true;
    private bool canMoveLeft;
    private bool canMoveRight;
    private DIRECT cDirect = DIRECT.NONE;
    #endregion

    #region Unity Methods
    private void Awake()
    {
        centerEye.TriggerEnter += HandleTriggerEnterCenterTile;
        leftEye.TriggerEnter += HandleTriggerEnterLeftTile;
        rightEye.TriggerEnter += HandleTriggerEnterRightTile;
        waterFront.TriggerStay += HandleTriggerStayFront;
        waterFront.TriggerExit += HandleTriggerExitFront;
        waterLeft.TriggerStay += HandleTriggerStayLeft;
        waterLeft.TriggerExit += HandleTriggerExitLeft;
        waterRight.TriggerStay += HandleTriggerStayRight;
        waterRight.TriggerExit += HandleTriggerExitRight;
        groundInRange = new Dictionary<int, Tile>();
    }

    private void Start()
    {
        StartCoroutine(MyUpdate());
    }
    private void OnDestroy()
    {
        centerEye.TriggerEnter -= HandleTriggerEnterCenterTile;
        leftEye.TriggerEnter -= HandleTriggerEnterLeftTile;
        rightEye.TriggerEnter -= HandleTriggerEnterRightTile;
        waterFront.TriggerStay -= HandleTriggerStayFront;
        waterFront.TriggerExit -= HandleTriggerExitFront;
        waterLeft.TriggerStay -= HandleTriggerStayLeft;
        waterLeft.TriggerExit -= HandleTriggerExitLeft;
        waterRight.TriggerStay -= HandleTriggerStayRight;
        waterRight.TriggerExit -= HandleTriggerExitRight;
    }

    
    #endregion

    #region Public Methods
    public void Active(int level)
    {
        gameObject.SetActive(true);
        waterInFront = false;
        canMoveFront = true;
        canMoveLeft = canMoveRight = false;
        isActive = false;
        groundInRange.Clear();
        _level = level;
        isActive = true;
    }

    public void Deactive()
    {
        isActive = false;
        gameObject.SetActive(false);
    }
    public Vector2 Direct
    {
        get {
            if (cDirect == DIRECT.NONE || cDirect == DIRECT.FRONT)
                return new Vector2(0, 1f).normalized;
            else if (cDirect == DIRECT.LEFT)
                return new Vector2(-1f, 0.5f).normalized;
            else
                return new Vector2(1f, 0.5f).normalized;
        }
    }
    #endregion

    #region Private Methods
    private IEnumerator MyUpdate()
    {
        while (true)
        {
            if (isActive)
            {
                if (waterInFront)
                {
                    if (canMoveLeft && !waterInLeft)
                    {
                        MoveLeft();
                    }
                    else if (canMoveRight && !waterInRight)
                    {
                        MoveRight();
                    }
                    else
                    {
                        Jump();
                    }
                }
                else
                {
                    if (canMoveFront)
                    {
                        MoveFront();
                    }
                    else
                    {
                        if (canMoveLeft && !waterInLeft)
                        {
                            MoveLeft();
                        }
                        else if (canMoveRight && !waterInRight)
                        {
                            MoveRight();
                        }
                        else
                        {
                            Jump();
                        }
                    }
                }
                yield return new WaitForSeconds(0.1f);
            }
        }        
    }
    private void MoveLeft()
    {
        if (cDirect != DIRECT.LEFT)
            Debug.Log("MoveLeft");
        cDirect = DIRECT.LEFT;
    }

    private void MoveRight()
    {
        if (cDirect != DIRECT.RIGHT)
            Debug.Log("MoveRight");
        cDirect = DIRECT.RIGHT;
    }

    private void MoveFront()
    {
        if (cDirect != DIRECT.FRONT)
            Debug.Log("MoveRight");
        cDirect = DIRECT.FRONT;
    }

    private void Jump()
    {
        Vector2 dir = new Vector2(0, -1).normalized;
    }
    private void HandleTriggerEnterCenterTile(object obj)
    {
        Collider col = ((Collider)obj);
        if (col.CompareTag("Tile"))
        {
            Tile tile = ((Collider)obj).gameObject.GetComponent<Tile>();
            if (tile != null)
            {
                if(tile.STATUS == TileStatus.Normal)
                {
                    canMoveFront = true;
                }else if(tile.STATUS == TileStatus.Broken)
                {
                    canMoveFront = false;
                }                
            }
        }  
    }

    private void HandleTriggerEnterLeftTile(object obj)
    {
        Collider col = ((Collider)obj);
        if (col.CompareTag("Tile"))
        {
            Tile tile = ((Collider)obj).gameObject.GetComponent<Tile>();
            if (tile != null)
            {
                if (tile.STATUS == TileStatus.Normal)
                {
                    canMoveLeft = true;
                }
                else if (tile.STATUS == TileStatus.Broken)
                {
                    canMoveLeft = false;
                }
            }
        }
    }

    private void HandleTriggerEnterRightTile(object obj)
    {
        Collider col = ((Collider)obj);
        if (col.CompareTag("Tile"))
        {
            Tile tile = ((Collider)obj).gameObject.GetComponent<Tile>();
            if (tile != null)
            {
                if (tile.STATUS == TileStatus.Normal)
                {
                    canMoveRight = true;
                }
                else if (tile.STATUS == TileStatus.Broken)
                {
                    canMoveRight = false;
                }
            }
        }
    }
       
    private void HandleTriggerStayFront(object obj)
    {
        Collider col = ((Collider)obj);
        if (col.CompareTag("Tile"))
        {
            waterInFront = false;
        }
       
    }

    private void HandleTriggerExitFront(object obj)
    {
        Collider col = ((Collider)obj);
        if (col.CompareTag("Tile"))
        {
            waterInFront = true;
        }
    }

    private void HandleTriggerStayLeft(object obj)
    {
        Collider col = ((Collider)obj);
        if (col.CompareTag("Tile"))
        {
            waterInLeft = false;
        }

    }

    private void HandleTriggerExitLeft(object obj)
    {
        Collider col = ((Collider)obj);
        if (col.CompareTag("Tile"))
        {
            waterInLeft = true;
        }
    }

    private void HandleTriggerStayRight(object obj)
    {
        Collider col = ((Collider)obj);
        if (col.CompareTag("Tile"))
        {
            waterInRight = false;
        }

    }

    private void HandleTriggerExitRight(object obj)
    {
        Collider col = ((Collider)obj);
        if (col.CompareTag("Tile"))
        {
            waterInRight = true;
        }
    }


    #endregion

    private enum DIRECT
    {
        FRONT,
        LEFT,
        RIGHT,
        NONE
    }
}
