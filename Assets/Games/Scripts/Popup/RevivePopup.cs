﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PopupSystem;
using UnityEngine.UI;

public class RevivePopup : SingletonPopup<RevivePopup>
{
    #region Inspector Variables
    public Text tvTime;
    public Image imgCountdown;
    #endregion

    #region Member Variables
    #endregion

    #region Public Methods
    public void Show()
    {
        base.Show();
    }

    public void Revive()
    {
        base.Hide(() => {
            GameManager.Instance.RevivePlayer();
        });
    }
    #endregion

    #region Private Methods
    #endregion
}
