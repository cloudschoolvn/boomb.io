﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    #region Inspector Variables
    public Transform mTrans, mBody;
    public Rigidbody mRigi;
    public PlayerAnim mAnim;
    public PlayerMovement movement;
    public ColliderEvent mCol;
    public SkinnedMeshRenderer mRender;
    public GameObject boom;
    public BotController bot;
    public bool isBot;
    #endregion

    #region Member Variables
    public int myID;
    private List<Boom> booms;
    private float lastTimeSpawn = 0;
    private bool isActive = false;
    private int _power = 0;
    public Queue<Boom> queue;
    #endregion

    #region Unity Methods
    private void Awake()
    {
        SimplePool.Preload(boom,5);
    }
    private void Start()
    {
        booms = new List<Boom>();
        queue = new Queue<Boom>();
        mCol.ColliderEnter += HandleColliderWater;
        myID = mCol.mCol.GetInstanceID();
    }
    private void OnDestroy()
    {
        mCol.ColliderExit -= HandleColliderWater;
    }
    private void Update()
    {
        if (isActive && Const.useBoom && (Time.timeSinceLevelLoad - lastTimeSpawn) > (1/Const.moveSpeed))
        {
            Vector3 pos = mTrans.position;
            pos.y = 0.5f;
            GameObject go = SimplePool.Spawn(boom, pos, Quaternion.Euler(Vector3.zero));
            Boom bm = go.GetComponent<Boom>();            
            if(queue.Count>(Const.baseNumberBoom + _power))
            {
                queue.Dequeue();                
            }
            queue.Enqueue(bm);
            bm.Init(this, _power);
            lastTimeSpawn = Time.timeSinceLevelLoad;
        }
    }

    #endregion

    #region Public Methods    
    public void Init(string _name, Vector3 pos)
    {
        pos.y = 0f;
        mBody.localPosition = Vector3.zero;
        mTrans.position = pos;
        gameObject.SetActive(true);
        if(!isBot)
            GameManager.Instance.SetCameraFollowPlayer(true);
    }

    public void Active()
    {
        mAnim.SetRun();
        movement.ActiveMove();
        isActive = true;
        if (isBot)
        {            
            bot.Active(1);
        }
            
    }

    public void Deactive()
    {
        isActive = false;
        movement.DeactiveMove();
        if (isBot)
            bot.Deactive();
        else
        {
            GameManager.Instance.SetCameraFollowPlayer(false);
        }
        gameObject.SetActive(false);
        GameEventManager.Instance.OnPlayerDie(this);        
    }
    public void PowUp()
    {
        _power += 1;
    }

    #endregion

    #region Private Methods
    private void HandleColliderWater(object obj)
    {
        if (((Collision)obj).collider.CompareTag("Water"))
        {
            if(isActive)
                Deactive();
        }

    }
    #endregion
}
