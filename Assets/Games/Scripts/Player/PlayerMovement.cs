﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    #region Inspector Variables   
    #endregion

    #region Member Variables
    private bool canMove = false;
    private Player player;
    private Vector2 direct = new Vector2(0, 1);
    #endregion

    #region Unity Methods
    private void Awake()
    {
        player = GetComponent<Player>();
    }
    private void Update()
    {
        if (canMove)
        {
            if (player.isBot)
            {
                if(player.bot.Direct.x !=0 && player.bot.Direct.y != 0)
                {
                    direct.x = player.bot.Direct.x;
                    direct.y = player.bot.Direct.y;
                }
            }
            else
            {
                if (GameUIManager.Instance.joystick.Horizontal != 0 && GameUIManager.Instance.joystick.Vertical != 0)
                {
                    direct.x = GameUIManager.Instance.joystick.Horizontal;
                    direct.y = GameUIManager.Instance.joystick.Vertical;                    
                }
            }

            float angle = Mathf.Atan2(direct.y, direct.x) * Mathf.Rad2Deg;
            player.mBody.localEulerAngles = new Vector3(0, 90 - angle, 0);
            Vector2 dist = (Const.moveSpeed * Time.deltaTime)*direct.normalized;
            player.mTrans.position += new Vector3(dist.x, 0, dist.y);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Const.moveSpeed = Mathf.Clamp(Const.moveSpeed + 0.5f, 1, 20);
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            Const.moveSpeed = Mathf.Clamp(Const.moveSpeed - 0.5f, 1, 20);
        }
    }
    #endregion

    #region Public Methods
    public void ActiveMove()
    {
        canMove = true;
    }
    public void DeactiveMove()
    {
        canMove = false;
    }
    #endregion

    #region Private Methods
    #endregion
}
