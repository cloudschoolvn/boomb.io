﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderEvent : MonoBehaviour
{
    public ActionEvent.OneParam TriggerEnter;
    public ActionEvent.OneParam TriggerExit;
    public ActionEvent.OneParam TriggerStay;
    public ActionEvent.OneParam ColliderEnter;
    public ActionEvent.OneParam ColliderExit;

    [HideInInspector]
    public BoxCollider mCol;
    private void Awake()
    {
        mCol = GetComponent<BoxCollider>();
    }

    private void OnDestroy()
    {
        TriggerEnter = null;
        TriggerExit = null;
        ColliderEnter = null;
        ColliderExit = null;
        TriggerStay = null;
    }

    private void OnTriggerEnter(Collider other)
    {
        TriggerEnter?.Invoke(other);
    }

    private void OnTriggerExit(Collider other)
    {
        TriggerExit?.Invoke(other);
    }

    private void OnTriggerStay(Collider other)
    {
        TriggerStay?.Invoke(other);
    }

    private void OnCollisionEnter(Collision collision)
    {
        ColliderEnter?.Invoke(collision);
    }

    private void OnCollisionExit(Collision collision)
    {
        ColliderExit?.Invoke(collision);
    }
}
