﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Boom : MonoBehaviour
{
    private Tile tile;
    private int masterID;
    public SphereCollider explode;
    private Tween tween;
    private Coroutine explodetask;
    private Player player;
    private MeshRenderer mRender;
    private bool isFirst = false;
    #region Unity Methods

    private void Awake()
    {
        mRender = GetComponent<MeshRenderer>();
    }
    private void OnCollisionEnter(Collision col)
    {
        if (col.collider.CompareTag("Tile"))
        {
            tile = col.collider.GetComponent<Tile>();
        }else
        if (col.collider.CompareTag("Player"))
        {
            int colId = col.collider.GetInstanceID();
            if(colId != masterID)
            {
                Player enemy = col.gameObject.GetComponent<Player>();
                enemy.Deactive();
                Debug.Log("Trigger Explode");
                if (explodetask != null)
                    StopCoroutine(explodetask);
                isFirst = true;
                StartCoroutine(Explode(0, true));
            }
        }
       
    }

    #endregion

    #region Public Methods
    public void Init(Player player,int power)
    {
        explode.radius = 0;
        this.player = player;
        this.masterID = player.myID;
        mRender.material = player.mRender.material;
        float timeExplode = (Const.baseNumberBoom * (1f / 4f)) + power * 0.2f;
        explodetask = StartCoroutine(Explode(timeExplode,false));
    }

    public void ForceExplode(int id)
    {
        if (masterID != id)
            return;
        if (explodetask != null)
            StopCoroutine(explodetask);
        isFirst = false;
        if (gameObject.activeSelf)
            StartCoroutine(Explode(0, true));
    }
    #endregion

    #region Private Methods
    private IEnumerator Explode(float delay,bool isTrigger= false)
    {
        yield return new WaitForSeconds(delay);
        
        if (isTrigger)
        {
            //Debug.Log("IsTrigger");
            //float temp = 0;
            //tween = DOTween.To(() => temp, x => temp = x, 2f, 0.3f).OnUpdate(() =>
            //{
            //    explode.radius = temp;
            //}).OnComplete(() =>
            //{
            //    if (tile != null)
            //    {
            //        tile.OnTouch(masterID, 0.1f, 2f);
            //        tile = null;
            //    }
            //    SimplePool.Despawn(this.gameObject);
            //});

            if (tile != null)
            {
                tile.OnTouch(masterID, 0.1f, 2f);
                tile = null;
            }
            if (isFirst)
            {
                List<Boom> lst = new List<Boom>(player.queue.ToArray());
                //Debug.Log(lst.Count);
                foreach (Boom bo in lst)
                {
                    if (bo != null)
                        bo.ForceExplode(masterID);
                };
            }
            masterID = -1;
            tile = null;
            SimplePool.Despawn(this.gameObject);
        }
        else
        {
            masterID = -1;
            tile = null;
            SimplePool.Despawn(this.gameObject);
        } 
    }
    #endregion
}
