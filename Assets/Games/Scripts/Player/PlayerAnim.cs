﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnim : MonoBehaviour
{
    #region Inspector Variables
    #endregion

    #region Member Variables
    private Animator animator;
    #endregion

    #region Unity Methods
    private void Awake()
    {
        animator = GetComponent<Animator>();
    }
    #endregion

    #region Public Methods
    public void SetIdle()
    {
        animator.SetTrigger("isIdle");
    }

    public void SetRun()
    {
        animator.SetTrigger("isRun");
    }

    public void SetDance(Action callback = null)
    {
        animator.SetTrigger("isDance");
    }

    public void SetDead(Action callback = null)
    {
        animator.SetTrigger("isDead");
    }
    #endregion

    #region Private Methods
    #endregion
}
