﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    #region Member Variables    
    public Transform mTrans;
    private MeshRenderer mRender;
    private BoxCollider mCol;
    public Material norMat, warMat;
    private int triggerID = -1;
    private TileStatus status;
    private int mID;
    #endregion

    #region Unity Methods
    private void Awake()
    {
        mTrans = GetComponent<Transform>();
        mRender = GetComponent<MeshRenderer>();
        mCol = GetComponent<BoxCollider>();
        status = TileStatus.Normal;
        mID = mCol.GetInstanceID();
    }

    private void OnCollisionExit(Collision col)
    {
        if (!Const.useBoom)
            OnTouch(-1, 1f, 3);
    }

    private void OnTriggerEnter(Collider col)
    {
        status = TileStatus.Touched;
        if (col.CompareTag("Player"))
        {
            Player enemy = col.gameObject.GetComponent<PlayerBody>()?.player;
            if (enemy != null)
            {
                enemy.Deactive();
                //Debug.Log("Bot " + col.GetInstanceID() + " die");
            }
        }
    }
    #endregion

    #region Public Methods
    public TileStatus STATUS
    {
        get { return status; }
    }

    public void OnTouch(int triggerID, float timeActive, float timeExist)
    {
        this.triggerID = triggerID;
        StartCoroutine(PrepareHide(timeActive, timeExist));
    }
    public int ID
    {
        get { return mID; }
    }
    #endregion

    #region Private Methods
    private IEnumerator PrepareHide(float timeDelay, float timeExist)
    {
        mRender.material = warMat;
        //while (timeDelay > 0)
        //{            
        //    yield return new WaitForSeconds(0.2f);
        //    timeDelay -= 0.2f;
        //}
        yield return new WaitForSeconds(timeDelay);
        status = TileStatus.Broken;
        mRender.enabled = false;
        mCol.isTrigger = true;
        yield return new WaitForSeconds(timeExist);
        mRender.material = norMat;
        mRender.enabled = true;
        mCol.isTrigger = false;
        triggerID = -1;
        status = TileStatus.Normal;
    }
    #endregion
}
public enum TileStatus
{
    Normal,
    Broken,
    Touched
}
