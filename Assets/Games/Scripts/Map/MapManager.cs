﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapManager : SingletonMonoBehaviour<MapManager>
{
    #region Inspector Variables
    public List<Tile> tiles;
    #endregion

    #region Member Variables
    #endregion

    #region Unity Methods
    private void Awake()
    {
        instance = this;
    }
    #endregion

    #region Public Methods
    public Vector3 RandomPosition()
    {
        return tiles[Random.Range(0, tiles.Count)].mTrans.position;
    }

    public Tile RandomTile()
    {
        return tiles[Random.Range(0, tiles.Count)];
    }
    #endregion

    #region Private Methods
    #endregion
}
